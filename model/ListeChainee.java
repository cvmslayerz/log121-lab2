package model;
/******************************************************
Cours:   LOG121
Session: A2014
Groupe: 5
Projet: Laboratoire #2
�tudiant(e)s: Etienne Caya, Dominique Lauzon,
Nicolas Poitras
              
              
Professeur : Francis Cardinal
Nom du fichier: Rectangle.java
Date cr��: 2014-09-16
Date dern. modif. 2014-09-16
*******************************************************
Historique des modifications
*******************************************************
2014-09-16 Version initiale
2014-09-19 Socket ferm� correctement en cas d'arr�t d'ex�cution
*******************************************************/
import geo.AbstractForme;

public class ListeChainee {
	
	public static final String CROISSANT = "cr";
	public static final String DECROISSANT = "dcr";
	private final int TAILLE_MAX = 10;
	private int tailleListe = 0;
	private Noeud laliste, iterateur;

	/**
	 * Liste chainee est une implementation maison a but pedagogique d'une liste chainee. 
	 */
	public ListeChainee(){
		
	}
	/**
	 * permet d'ajouter une forme a la liste
	 * @param f forme a ajouter a la liste
	 */
	public void ajouter(AbstractForme forme){
		
		Noeud nouveauNoeud = new Noeud(forme);
		
		if(this.tailleListe == TAILLE_MAX){
			this.viderListe();
			this.tailleListe = 0;
		}
		
		if(laliste != null){
			
			Noeud dernier = trouverDernier();
			nouveauNoeud.setSuivant(null);
			dernier.setSuivant(nouveauNoeud);
			dernier=nouveauNoeud;
		}
		else{
			nouveauNoeud.setSuivant(null);
			laliste = nouveauNoeud;
		}
		
		this.tailleListe++;
	}
	
	/**
	 * trouve le dernier noeud de la liste
	 */
	private Noeud trouverDernier(){
		Noeud ptr1 = laliste;
		while(ptr1.getSuivant() != null){
			ptr1 = ptr1.getSuivant();
		}
		return ptr1;
	}
	
	/**
	 * 
	 * @param n noeud dont on cherche le precedent
	 * @return le noeud precedent
	 */
	private Noeud trouverPrecedent(Noeud noeudCourant){
		Noeud prePtr = laliste;
		while(prePtr.getSuivant() != noeudCourant){
			prePtr = prePtr.getSuivant();
		}
		
		return prePtr;
	}
	
	/**
	 * tri la liste chainee en ordre de l'aire de la forme
	 * @param typeDeTri constante de la classe liste chainee. soit CROISSANT ou DECROISSANT
	 */
	public void trierAire(String typeDeTri){
		Noeud dernier = trouverDernier();
		Noeud ptr1 = laliste;
		Noeud ptr2;
		while(ptr1 != dernier){
			ptr2 = ptr1.getSuivant();
			if(typeDeTri == CROISSANT){
				if(ptr1.getForme().getAire() > ptr2.getForme().getAire()){
					echangerNoeuds(ptr1, ptr2);
					ptr1 = laliste;
				}
					
				else{
					ptr1 = ptr1.getSuivant();
				}
			}

			else if(typeDeTri == DECROISSANT){
				if(ptr1.getForme().getAire() < ptr2.getForme().getAire()){
					echangerNoeuds(ptr1, ptr2);
					ptr1 = laliste;
				}
					
				else{
					ptr1 = ptr1.getSuivant();
				}
			}
		}
	}
	
	/**
	 * tri la liste chainee selon la distance la plus longue entre deux points des formes
	 */
	public void trierDistance(){
		Noeud dernier = trouverDernier();
		Noeud ptr1 = laliste;
		Noeud ptr2;
		while(ptr1 != dernier){
			ptr2 = ptr1.getSuivant();
			if(ptr1.getForme().getDistance() > ptr2.getForme().getDistance()){
				echangerNoeuds(ptr1, ptr2);
				ptr1 = laliste;
			}
				
			else{
				ptr1 = ptr1.getSuivant();
			}
		}		
	}
	
	/**
	 * tri selon le type de forme dans l'ordre( carre rectangle cercle ovale ligne)
	 * @param typeDeTri constante de la classe liste chainee. soit CROISSANT ou DECROISSANT
	 */
	public void trierType(String typeDeTri){
		Noeud dernier = trouverDernier();
		Noeud ptr1 = laliste;
		Noeud ptr2;
		while(ptr1 != dernier){
			ptr2 = ptr1.getSuivant();
			if(typeDeTri == CROISSANT){
				if(ptr1.getForme().getFormeValue() > ptr2.getForme().getFormeValue()){
					echangerNoeuds(ptr1, ptr2);
					ptr1 = laliste;
				}
					
				else{
					ptr1 = ptr1.getSuivant();
				}
			}
			else{
				if(ptr1.getForme().getFormeValue() < ptr2.getForme().getFormeValue()){
					echangerNoeuds(ptr1, ptr2);
					ptr1 = laliste;
				}
					
				else{
					ptr1 = ptr1.getSuivant();
				}
			}
			
		}	
	}
	
	/**
	 * tri selon le numero sequenciel de chaque forme
	 * @param typeDeTri constante de la classe liste chainee. soit CROISSANT ou DECROISSANT
	 */
	public void trierNseq(String typeDeTri){
		Noeud dernier = trouverDernier();
		Noeud ptr1 = laliste;
		Noeud ptr2;
		while(ptr1 != dernier){
			ptr2 = ptr1.getSuivant();
			if(typeDeTri == CROISSANT){
				if(ptr1.getForme().getNseq() > ptr2.getForme().getNseq()){
					echangerNoeuds(ptr1, ptr2);
					ptr1 = laliste;
				}
					
				else{
					ptr1 = ptr1.getSuivant();
				}
			}
			else{
				if(ptr1.getForme().getNseq() < ptr2.getForme().getNseq()){
					echangerNoeuds(ptr1, ptr2);
					ptr1 = laliste;
				}
					
				else{
					ptr1 = ptr1.getSuivant();
				}
			}
		}	
	}

	/**
	 * echange deux noeuds dans a liste chainee
	 * @param n1 le premier noeud a echanger
	 * @param n2 le noeud suivant le premier noeud
	 */
	private void echangerNoeuds(Noeud noeud1, Noeud noeud2){
		Noeud dernier = trouverDernier();
		Noeud prePtr;
		if(noeud1 == laliste){
			noeud1.setSuivant(noeud2.getSuivant());
			noeud2.setSuivant(noeud1);
			laliste = noeud2;
		}	
		else if(noeud2 == dernier ){
			prePtr = trouverPrecedent(noeud1);
			prePtr.setSuivant(noeud2);
			noeud1.setSuivant(null);
			noeud2.setSuivant(noeud1);
			dernier=noeud1;
		}
		else{
			prePtr = trouverPrecedent(noeud1);
			noeud1.setSuivant( noeud2.getSuivant());
			noeud2.setSuivant(noeud1);
			prePtr.setSuivant(noeud2);
		}
	}
	
	/**
	 * place le pointeur iterateur sur le debut de la liste
	 */
	public void commencerIteration(){
		iterateur = laliste;
	}
	/**
	 * avance l'iterateur au noeud suivant
	 */
	public void avancerIteration(){
		iterateur = iterateur.getSuivant();
	}
	/**
	 * 
	 * @return le noeud actuel ou l'iterateur est plac�
	 */
	public Noeud getNoeudCourant() {
		return iterateur;
	}
	
	/**
	 * 
	 * @return la taille de la liste
	 */
	public int getTailleListe() {
		return this.tailleListe;
	}
	/**
	 * coupe la liste chainee afin de la vider
	 */
	private void viderListe(){
		laliste = null;
	}
}
