package model;
/******************************************************
Cours:   LOG121
Session: A2014
Groupe: 5
Projet: Laboratoire #2
�tudiant(e)s: Etienne Caya, Dominique Lauzon,
Nicolas Poitras
              
              
Professeur : Francis Cardinal
Nom du fichier: Rectangle.java
Date cr��: 2014-09-16
Date dern. modif. 2014-09-16
*******************************************************
Historique des modifications
*******************************************************
2014-09-14 Version initiale
2014-09-16 Connexion avec le serveur se fait maintenant avec CommWorker
*******************************************************/

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import ca.etsmtl.log.util.IDLogger;
/**
 * Classe qui fait la gestion des instructions recues par l'interface graphique, des messages recus par le serveur,
 * des objets de la classe Forme r�sultant de ces instructions et de l'alerte aux interfaces graphiques pour la mise-�-jour
 * des formes visuelles.
 * @author Etienne Caya
 */
public class CommGestion {

	private ListeChainee listeFormes;
	private final String pattern;
	private boolean connectionActive = false;
	
	private CommWorker connecteur;
	private PropertyChangeSupport alerteur = null;
	
	
	public CommGestion() {
		//Pattern qui retourne les infos n�cessaires du message
		this.pattern = "^(\\d.*?)\\s<(.*?)>\\s(.*?)\\s</";
		this.listeFormes = new ListeChainee();
		this.alerteur = new PropertyChangeSupport(this);
	}
	/**
	 * D�bute la communication avec le serveur exig� par l'utilisateur
	 * @param commande cha�ne de caract�res contenant le nom et le port du serveur (sous forme "nom:port")
	 */
	public void startComm(String commande) {
		if(commande != null && commande.length()> 0){
			String[] result = commande.split(":");
			if(result.length == 2){
				try {
				String nomServeur = result[0];
				int portServeur = Integer.parseInt(result[1]);
				this.connecteur = new CommWorker(this, nomServeur , portServeur);
				//Part le thread
				this.connecteur.execute();
				//D�finit le listener � utiliser si firePropertyChange est appel� durant la boucle
				//his.connecteur.addPropertyChangeListener(this.ecouteur);
				this.connectionActive = true;
				}
				catch(NumberFormatException e) {
					e.printStackTrace();
					//TODO Afficher message d'erreur
				}
			}
		}

	}
	/**
	 * Arr�te le fil d'ex�cution du connecteur (CommWorker),
	 * red�finit la connection comme �tant inactive et
	 * appel la m�thode du connecteur pour fermer le socket.
	 */
	public void stopComm() {
		if(this.connecteur != null){
			this.connecteur.cancel(true);
			this.connectionActive = false;
			this.connecteur.fermerConnection();
		}
	}
	/**
	 * Proc�de au traitement du message re�u par le connecteur en 3 �tapes :
	 * 1. Demande au parseur de s�parer les infos selon sa propre expression r�guli�re
	 * 2. Envoi les infos r�sultantes au CreateurFormes pour r�cup�rer l'instance de la forme correspondante
	 * 3. Alerte l'interface graphique que l'affichage des formes doit �tre mis-�-jour
	 * @param message cha�ne de caract�res re�u du serveur
	 */
	public void traiterMsg(String message) {
		//Separe les informations de l'objet selon le pattern d�finit
		String[] msgtraduit = Parseur.getMsgTraduit(this.pattern,message);
		
		//On traduit les données dans les types utilisés par CreateurFormes
		int nseq = Integer.parseInt(msgtraduit[0]);
		
		String type = msgtraduit[1];
		
		String[] coordString = msgtraduit[2].split("\\s");
		int[] coord = new int[coordString.length];
		
		//Boucle qui transfert les coordonn�es du message en int
		for(int i = 0; i < coord.length; i++){
			coord[i] = Integer.parseInt(coordString[i]);
		}

		//ajoute la nouvelle forme recu � l'index courant
		this.listeFormes.ajouter(CreateurFormes.creerForme(type, coord, nseq));
		
		//Ajout de v�rification des num�ros de s�quences � IDLogger
		IDLogger logger = IDLogger.getInstance();
		logger.logID(nseq);
		
		//Si jamais le tableau est rempli, on arrete la communication avec le serveur
		if(this.listeFormes.getTailleListe() == 10) {
			//Alerte l'�couteur du GUI, pour provoquer une mise-a-jour de l'affichage, tout en changeant de thread
			this.listeFormes.trierNseq(ListeChainee.CROISSANT);
			this.alerteur.firePropertyChange("MISE-A-JOUR", null, null);
			this.stopComm();
		}
	}
	/**
	 * Affiche les formes selon leur numéro séquence décroissant 
	 */
	public void tri(String sorte){
		switch(sorte){
			case "nseqCroissant": this.listeFormes.trierNseq(ListeChainee.CROISSANT);
				break;
			case "nseqDecroissant": this.listeFormes.trierNseq(ListeChainee.DECROISSANT);
				break;
			case "aireCroissant": this.listeFormes.trierAire(ListeChainee.CROISSANT);
				break;
			case "aireDecroissant": this.listeFormes.trierAire(ListeChainee.DECROISSANT);
				break;
			case "formeNormal": this.listeFormes.trierType(ListeChainee.CROISSANT);
				break;
			case "formeInverse": this.listeFormes.trierType(ListeChainee.DECROISSANT);
				break;
			case "distance" : this.listeFormes.trierDistance();
				break;
			default: this.listeFormes.trierNseq(ListeChainee.CROISSANT);
		}
		this.alerteur.firePropertyChange("MISE-A-JOUR", null, null);
	}
	
	
	/**
	 * D�finir le r�cepteur de l'information re�ue dans la communication avec le serveur
	 * @param listener sera alert� lors de l'appel de "firePropertyChanger" par la m�thode traiterMsg.
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener){
		this.alerteur.addPropertyChangeListener(listener);
	}
	
	/**
	 * @return si la connection du serveur est active, c'est-�-dire si CommWorker est en ex�cution
	 */
	public boolean isConnectionActive() {
		return this.connectionActive;
	}
	
	public boolean isListePleine() {
		if(this.listeFormes != null) {
			return true;
		}
		else
			return false; 
	}
	
	/**
	 * @return la liste des formes qui ont �t� cr��es suite � la r�ception des messages du serveur
	 */
	public ListeChainee getListeFormes() {
		return this.listeFormes;
	}

}
