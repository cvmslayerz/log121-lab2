package model;
/******************************************************
Cours:   LOG121
Session: A2014
Groupe: 5
Projet: Laboratoire #2
�tudiant(e)s: Etienne Caya, Dominique Lauzon,
Nicolas Poitras
              
              
Professeur : Francis Cardinal
Nom du fichier: Rectangle.java
Date cr��: 2014-09-16
Date dern. modif. 2014-09-16
*******************************************************
Historique des modifications
*******************************************************
2014-09-16 Version initiale
2014-09-19 Modifi�e pour accepter n'importe quel pattern
*******************************************************/

/**
 * Classe qui s'occupe de diviser une cha�ne de caract�res
 * selon l'expression r�guli�re que l'on lui fournie.
 * @author Etienne Caya
 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parseur {
	
	/**
	 * � partir de l'expression r�guli�re fournie en param�tre,
	 * on sous-divise la cha�ne et on retourne le r�sultat
	 * @param pattern l'expression r�guli�re � utiliser pour diviser la cha�ne
	 * @param message le message � diviser
	 * @return le tableau contenant chacun des cha�nes r�sultantes de l'expression r�guli�re
	 */
	public static final String[] getMsgTraduit(String pattern, String message) {
		try {
			Pattern patternObject = Pattern.compile(pattern);
			Matcher match = patternObject.matcher(message);
			
	
			if(match.find()) {
				String msg[] = new String[match.groupCount()];
				//Pour chaque match trouv�
				for(int j = 0; j < msg.length; j++) {
					msg[j] = match.group(j+1);//On l'ajoute au tableau � retourner
				}
				
				return msg;
			}
		} catch(Exception e) {
			
			e.printStackTrace();
		}
		return null;
	}
}
