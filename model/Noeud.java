package model;
/******************************************************
Cours:   LOG121
Session: A2014
Groupe: 5
Projet: Laboratoire #2
�tudiant(e)s: Etienne Caya, Dominique Lauzon,
Nicolas Poitras
              
              
Professeur : Francis Cardinal
Nom du fichier: Rectangle.java
Date cr��: 2014-09-16
Date dern. modif. 2014-09-16
*******************************************************
Historique des modifications
*******************************************************
2014-09-29 Version initiale
*******************************************************/
import geo.AbstractForme;
/**
 * Classe repr�sentant la structure (noeud) d'une liste chain�e simple.
 * Elle est form�e d'un �l�ment Forme et d'une r�f�rence � l'�l�ment suivant
 * @author Dominique Lauzon
 *
 */
public class Noeud {
	
	 private AbstractForme forme;
	 private Noeud suivant;
	 
	 
	 public Noeud(AbstractForme forme){
		 this.forme = forme;
		 
	 }

	 /**
	  * retourne la forme dans le noeud
	  * @return AbstractForme f
	  */
	public AbstractForme getForme() {
		return this.forme;
	}

	/**
	 * 
	 * @return AbstractForme suivante
	 */
	public Noeud getSuivant() {
		return suivant;
	}

	/**
	 * Modifie la valeur dont est suivi la AbstractForme par une autre AbstractForme
	 * @param suivant
	 */
	public void setSuivant(Noeud suivant) {
		this.suivant = suivant;
	}
	

	 
}
