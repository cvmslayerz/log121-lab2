package model;
import geo.Cercle;
import geo.AbstractForme;
import geo.Ligne;
import geo.Ovale;
import geo.Rectangle;

/******************************************************
Cours:   LOG121
Session: A2014
Groupe: 5
Projet: Laboratoire #2
�tudiant(e)s: Etienne Caya, Dominique Lauzon,
Nicolas Poitras
              
              
Professeur : Francis Cardinal
Nom du fichier: Rectangle.java
Date cr��: 2014-09-16
Date dern. modif. 2014-09-16
*******************************************************
Historique des modifications
*******************************************************
2014-09-18 Version initiale
*******************************************************/

/**
 * Classe statique qui cr�e la forme selon les param�tres donn�s
 * @author Etienne Caya
 *
 */
public final class CreateurFormes {
	

	
	/**
	 * Cr�e l'instance d'une sous-classe de Forme selon les informations fournies
	 * @param type la classe de la forme � cr�er
	 * @param coord les attributs de la forme
	 * @return une instance d'une des sous classes de Forme cr�� � partir des param�tres pr�c�dents
	 */
	public static AbstractForme creerForme(String type, int[] coord, int nseq) {
		
		AbstractForme nouvForm;
		
		switch(type){
			case "CERCLE": nouvForm = new Cercle(type,coord, nseq);
					break;
			case "OVALE": nouvForm = new Ovale(type,coord, nseq);
					break;
			case "LIGNE": nouvForm = new Ligne(type,coord, nseq);
					break;
			default: nouvForm = new Rectangle(type,coord, nseq);
		}
		

		return nouvForm;
	}
}
