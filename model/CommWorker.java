package model;
/******************************************************
Cours:   LOG121
Session: A2014
Groupe: 5
Projet: Laboratoire #2
�tudiant(e)s: Etienne Caya, Dominique Lauzon,
Nicolas Poitras
              
              
Professeur : Francis Cardinal
Nom du fichier: Rectangle.java
Date cr��: 2014-09-16
Date dern. modif. 2014-09-16
*******************************************************
Historique des modifications
*******************************************************
2014-09-16 Version initiale
2014-09-19 Socket ferm� correctement en cas d'arr�t d'ex�cution
*******************************************************/

import javax.swing.SwingWorker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Classe qui s'occupe d'�tablir une connection avec le serveur
 * et effectue la communication avec celui-ci dans un fil d'ex�cution s�par�
 * @author Etienne Caya
 */
public class CommWorker extends SwingWorker<Object, Object>{
	
	private final int DELAI = 0;
	
	private Socket connection;
	private BufferedReader filRecu;
	private PrintWriter filEnvoye;
	
	private CommGestion gestionnaire;
	
	private String nomServeur;
	private int portServeur;
	
	/**
	 * Constructeur
	 * Cr�e la communication imm�diatement avec le serveur, si possible
	 * @param gestionnaire r�f�rence � CommGestion
	 * @param nomServeur nom du serveur demand�
	 * @param portServeur port du serveur demand�
	 */
	public CommWorker(CommGestion gestionnaire, String nomServeur, int portServeur){
		this.gestionnaire = gestionnaire;
		this.nomServeur = nomServeur;
		this.portServeur = portServeur;
		
		try {
			
			this.connection = new Socket( InetAddress.getByName(this.nomServeur) , this.portServeur );
			//filRecu est en fait un InputStreamReader qui est transpos� en BufferedReader, pour �viter de forcer l'attente d'une lecture
			this.filRecu = new BufferedReader(new InputStreamReader (connection.getInputStream()));
			this.filEnvoye = new PrintWriter(connection.getOutputStream());
			
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Envoie un message de fermeture au serveur et ferme le socket
	 * qui sert de connexion.
	 */
	public void fermerConnection(){
		try {
			this.filEnvoye.println("END");
			this.filEnvoye.flush();
			this.connection.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Envoie et recoi les messages avec le serveur, puis envoie le message recu
	 * au gestionnaire (les actions contenu dans cette m�thode se produise dans un fil d'ex�cution
	 * diff�rent au reste du programme)
	 */
	@Override
	protected Object doInBackground() throws Exception {
		try {
			
			while(true) {
				Thread.sleep(DELAI);
				
				
				this.filEnvoye.println("GET");//Entre la commande � envoyer au serveur
				this.filEnvoye.flush();//Effectue l'envoi de la commande
				
				this.filRecu.readLine(); //Pour ignorer la ligne "commande>"
				String nouveauMsg = filRecu.readLine(); //recoit le message de la forme
				
				gestionnaire.traiterMsg(nouveauMsg);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
