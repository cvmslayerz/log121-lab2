package geo;

/******************************************************
Cours:   LOG121
Session: A2014
Groupe: 5
Projet: Laboratoire #2
�tudiant(e)s: Etienne Caya, Dominique Lauzon,
Nicolas Poitras
              
              
Professeur : Francis Cardinal
Nom du fichier: Rectangle.java
Date cr��: 2014-09-16
Date dern. modif. 2014-09-16
*******************************************************
Historique des modifications
*******************************************************
2014-09-16 Version initiale
*******************************************************/

/**
 * Class qui repr�sente une ligne � partir de deux points
 * @author Etienne
 *
 */
public class Ligne extends AbstractForme {
	private int[] pointDebut = new int[2];
	private int[] pointFin = new int[2];
	
	public Ligne(String type, int[] coord, int nseq) {
		super(type, nseq);
		
		this.pointDebut[0] = coord[0];//x1
		this.pointDebut[1] = coord[1];//y1
		
		this.pointFin[0] = coord[2];//x2
		this.pointFin[1] = coord[3];//y2
		
		//longueur x2-x1
		//hauteur y2-y1
		
		calculAire();
		calculDistance();
	}
	
	public int[] getPointDebut(){
		return this.pointDebut;
	}
	
	public int[] getPointFin() {
		return this.pointFin;
	}
	
	@Override
	protected void calculAire(){
		this.aire = 0;
	}
	
	@Override
	protected void calculDistance(){
		this.distance = (int) Math.sqrt(Math.pow(pointDebut[0]-pointFin[0],2)+Math.pow(pointDebut[1]- pointFin[1],2));
	}
}
