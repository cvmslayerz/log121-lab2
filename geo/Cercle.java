package geo;

/******************************************************
Cours:   LOG121
Session: A2014
Groupe: 5
Projet: Laboratoire #2
�tudiant(e)s: Etienne Caya, Dominique Lauzon,
Nicolas Poitras
              
              
Professeur : Francis Cardinal
Nom du fichier: Rectangle.java
Date cr��: 2014-09-16
Date dern. modif. 2014-09-16
*******************************************************
Historique des modifications
*******************************************************
2014-09-16 Version initiale
2014-09-18 Changement d'h�ritage (extends Forme plutot que Ovale)
*******************************************************/ 

/**
 * Classe qui repr�sente un cercle
 * @author Etienne Caya
 */
public class Cercle extends AbstractForme {
	protected int rayonH; //rayon horizontal
	protected int[] pointCentre = new int[2];
	
	/**
	 * Constructeur
	 * @param type type de l'instance
	 * @param coord attributs/coordonn�es de l'instance
	 */
	public Cercle(String type, int[] coord, int nseq) {
		super(type, nseq);
		
		//Coordonn�es du centre du cercle
		this.pointCentre[0] = coord[0];
		this.pointCentre[1] = coord[1];
		
		//Rayon
		this.rayonH = coord[2];
		
		calculAire();
		calculDistance();
		
	}
	/**
	 * @return le point au centre du cercle
	 */
	public int[] getPointCentre() {
		return this.pointCentre;
	}
	
	/**
	 * @return le rayon horizontal du cercle
	 */
	public int getRayonH() {
		return this.rayonH;
	}
	
	/**
	 * calcul de l'aire du cercle
	 */
	@Override
	protected void calculAire(){
		this.aire = (int) (Math.PI*(Math.pow(rayonH,2)));
	}
	
	@Override
	protected void calculDistance(){
		this.distance = rayonH*2;
	}
	
}
