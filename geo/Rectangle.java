package geo;

/******************************************************
Cours:   LOG121
Session: A2014
Groupe: 5
Projet: Laboratoire #2
�tudiant(e)s: Etienne Caya, Dominique Lauzon,
Nicolas Poitras
              
              
Professeur : Francis Cardinal
Nom du fichier: Rectangle.java
Date cr��: 2014-09-16
Date dern. modif. 2014-09-16
*******************************************************
Historique des modifications
*******************************************************
2014-09-16 Version initiale
*******************************************************/

/**
 * Class qui repr�sente un rectangle
 * @author Etienne Caya
 *
 */
public class Rectangle extends AbstractForme {
	private int[] pointDebut = new int[2];
	private int longueur;
	private int hauteur;
	
	/**
	 * Constructeur
	 * @param type type de l'instance
	 * @param coord attributs/coordonn�es de l'instance
	 */
	public Rectangle(String type, int[] coord, int nseq){
		super(type, nseq);
		
		this.pointDebut[0] = coord[0];
		this.pointDebut[1] = coord[1];
		
		this.longueur = coord[3] - coord[1];
		this.hauteur = coord[2] - coord[0];
		
		calculAire();
		calculDistance();
	}
	
	/**
	 * @return le point du sommet en haut � gauche du rectangle
	 */
	public int[] getPointDebut(){
		return this.pointDebut;
	}
	
	/**
	 * @return la longueur du rectangle
	 */
	public int getLongueur() {
		return this.longueur;
	}
	
	/**
	 * @return la largeur du rectangle
	 */
	public int getHauteur() {
		return this.hauteur;
	}
	
	@Override
	protected void calculAire(){
		this.aire = longueur*hauteur;
	}
	
	@Override
	protected void calculDistance(){
		this.distance = (int) Math.sqrt(Math.pow(longueur, 2) + Math.pow(hauteur, 2));
	}
	
}
