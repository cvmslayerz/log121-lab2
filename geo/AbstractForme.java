package geo;
/******************************************************
Cours:   LOG121
Session: A2014
Groupe: 5
Projet: Laboratoire #2
�tudiant(e)s: Etienne Caya, Dominique Lauzon,
Nicolas Poitras
              
              
Professeur : Francis Cardinal
Nom du fichier: Rectangle.java
Date cr��: 2014-09-16
Date dern. modif. 2014-09-16
*******************************************************
Historique des modifications
*******************************************************
2014-09-16 Version initiale
*******************************************************/

/**
 * Class abstraite repr�sentant une forme g�om�trique
 * @author Etienne Caya
 */
public abstract class AbstractForme {
		protected String type;
		protected int aire;
		protected int distance;
		protected int nseq;
		
		/**
		 * Constructeur
		 * @param type type de l'instance
		 */
		public AbstractForme(String type, int nseq){
			this.type = type;
			this.nseq = nseq;
		}
		
		/**
		 * @return le type de la forme
		 */
		public String getType() {
			return this.type;
		}
		
		
		/**
		 * 
		 * @return le numero de sequence de la forme
		 */
		public int getNseq() {
			return nseq;
		}

		/**
		 * @return l'aire de la forme
		 */
		public int getAire() {
			return aire;
		}
		/**
		 * @return la distance entre les 2 points les plus eloign�es de la forme
		 */
		public int getDistance() {
			return distance;
		}

		/**
		 * Methode que toutes les formes auront a implementer pour calculer l'aire de cel;les-ci
		 */
		protected void calculAire(){
			
		}
		
		/**
		 * M�thode de calcul de la plus grande distance entre deux points
		 */
		protected void calculDistance(){
			
		}
		
		public int getFormeValue(){
			
			if(type.matches("CARRE"))
				return 0;
			else if(type.matches("RECTANGLE"))
				return 1;
			else if(type.matches("CERCLE"))
				return 2;
			else if(type.matches("OVALE"))
				return 3;
			else
				return 4;
			
		}
		
}
