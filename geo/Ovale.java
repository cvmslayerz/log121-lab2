package geo;

/******************************************************
Cours:   LOG121
Session: A2014
Groupe: 5
Projet: Laboratoire #2
�tudiant(e)s: Etienne Caya, Dominique Lauzon,
Nicolas Poitras
              
              
Professeur : Francis Cardinal
Nom du fichier: Rectangle.java
Date cr��: 2014-09-16
Date dern. modif. 2014-09-16
*******************************************************
Historique des modifications
*******************************************************
2014-09-16 Version initiale
2014-09-18 Changement d'heritage (extends Cercle plutot que Forme)
*******************************************************/ 


/**
 * Classe qui repr�sente un ovale
 * @author Etienne
 *
 */
public class Ovale extends Cercle {
	private int rayonV;
	
	/**
	 * Constructeur
	 * @param type type de l'instance
	 * @param coord attributs/coordonn�es de l'instance
	 */
	public Ovale (String type, int[] coord, int nseq) {
		super(type,coord, nseq);
		
		this.rayonV = coord[3];
		
		calculAire();
		calculDistance();
	}
	
	/**
	 * @return le rayon vertical de l'ovale
	 */
	public int getRayonV() {
		return this.rayonV;
	}
	
	@Override
	protected void calculAire(){
		this.aire = (int) Math.PI*rayonV*rayonH;
	}
	
	@Override
	protected void calculDistance(){
		if((rayonH *2)>rayonV*2){
			distance = rayonH*2;
		}
		else{
			distance = rayonV*2;
		}
	}
}
