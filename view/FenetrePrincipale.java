package view;
/******************************************************
Cours:  LOG121
Projet: Squelette du laboratoire #1
Nom du fichier: FenetrePrincipale.java
Date cr��: 2013-05-03
*******************************************************
Historique des modifications
*******************************************************
*@author Patrice Boucher
2013-05-03 Version initiale
2014-09-19 L�g�re Modification par Etienne Caya
*******************************************************/  

import java.awt.BorderLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;

import model.CommGestion;
 
/**
 * Cette classe repr�sente la fen�tre principale de l'application 
 * @author Patrice Boucher
 * @date 2013/05/04
 */
public class FenetrePrincipale extends JFrame implements PropertyChangeListener{
	
	private static final long serialVersionUID = -1210804336046370508L;
	private FenetreFormes fenetreFormes;
	/**
	 * Constructeur
	 */
	public FenetrePrincipale(CommGestion comm){
		
		MenuFenetre menu = new MenuFenetre(comm);
		this.setLayout(new BorderLayout());
		this.add(menu, BorderLayout.NORTH); 
		this.fenetreFormes = new FenetreFormes(comm);
		this.add(fenetreFormes, BorderLayout.CENTER); // Ajoute la fen�tre de forme � la fen�tre principale
		this.pack(); // Ajuste la dimension de la fen�tre principale selon celle de ses composants
		this.setVisible(true); // Rend la fen�tre principale visible.
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * Demande � FenetreFormes de redessiner les formes de CommGestion
	 * lorsqu'un �v�nement PropertyChange est lanc� (dans ce cas l�, par CommGestion).
	 * @author Etienne Caya
	 */
	@Override
	public void propertyChange(PropertyChangeEvent event) {

		if(event.getPropertyName().equals("MISE-A-JOUR")) {
			this.fenetreFormes.repaint();
		}
	}
}
