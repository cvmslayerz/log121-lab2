package view;
/******************************************************
Cours:  LOG121
Projet: Squelette du laboratoire #1
Nom du fichier: FenetreFormes.java
Date cr��: 2013-05-03
*******************************************************
Historique des modifications
*******************************************************
*@author Patrice Boucher
2013-05-03 Version initiale
2014-09-19 Modification par Etienne Caya (affiche les formes)
2014-10-01 Modification par l'�quipe du laboraitoire #2
*******************************************************/  

import geo.Cercle;
import geo.AbstractForme;
import geo.Ligne;
import geo.Ovale;
import geo.Rectangle;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;

import model.CommGestion;
import model.ListeChainee;

/**
 * Cette fen�tre g�re l'affichage des formes
 * @author Patrice Boucher
 * @date 2013/05/04
 */
public class FenetreFormes extends JComponent{
	
	private static final long serialVersionUID = -2262235643903749505L;
	public static final int WIDTH = 500;
	public static final int HEIGHT = 500;
	public static final Dimension DIMENSION = new Dimension(500,500);
	private final static BasicStroke DASHED = new BasicStroke(1.0f,
		      BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, new float[]{ 5.0f }, 0.0f);
	private CommGestion comm;
		
	/**
	 * Constructeur
	 */
	public FenetreFormes(CommGestion comm){
		this.comm = comm;
	}
	
	/**
	 * Affiche l'interpr�tation visuelle des formes contenu dans la liste des formes
	 * de CommGestion. � partir du type de la forme, la m�thode d�termine la couleur, la distance
	 * et les autres param�tres n�cessaires � l'affichage de la forme.
	 * Couleurs : 
	 * Ligne = bleue
	 * Cercle = cyan
	 * Ovale = vert
	 * Rectangle = rouge
	 * Carre = jaune
	 * @param g contexte graphique de la fen�tre
	 * @author Etienne Caya
	 */
	@Override
	public void paint(Graphics gContext) {
		super.paint(gContext);
		
		Graphics2D gContextBorders = (Graphics2D) gContext.create();
		gContextBorders.setStroke(DASHED);
		
		ListeChainee listeFormes = this.comm.getListeFormes();
		if(listeFormes != null) {
			listeFormes.commencerIteration();
			for (int i = 0; i < listeFormes.getTailleListe() ; i++){
				AbstractForme forme = listeFormes.getNoeudCourant().getForme();
				//System.out.println(forme.getAire());
				if (forme != null) {
					if(forme.getType().equals("LIGNE")) {
						gContext.setColor(new Color(0,0,1,0.3f));//BLUE
						int[] pointDeb = ((Ligne)forme).getPointDebut();
						int[] pointFin = ((Ligne)forme).getPointFin();
						int longueur = pointFin[0] - pointDeb[0];
						int hauteur = pointFin[1] - pointDeb[1];
						int xDeb = i*40;
						int yDeb = i*40;
						int xFin = xDeb+longueur;
						int yFin = yDeb+hauteur;
						//System.out.println(i*40+longueur);
						//Pour bien dessiner le rectangle et la ligne a l'interieur du rectangle
						//il faut deplacer la ligne si celle-ci ne se deplace pas vers le coin en bas a droite de l'ecran
						if(longueur < 0) {
							xDeb+=Math.abs(longueur);
							xFin+=Math.abs(longueur);
						}
						if (hauteur < 0) {
							yDeb+=Math.abs(hauteur);
							yFin+=Math.abs(hauteur);
						}
						gContext.drawLine(xDeb, yDeb, xFin, yFin);
						gContextBorders.drawRect(i*40, i*40, Math.abs(longueur), Math.abs(hauteur));
					}
					else if(forme.getType().equals("CERCLE")){
						gContext.setColor(new Color(0,1,1,0.3f));//CYAN
						int rayon = ((Cercle)forme).getRayonH();
						gContext.fillOval(i*40, i*40, rayon*2, rayon*2);
						gContext.setColor(Color.BLACK);
						gContext.drawOval(i*40, i*40, rayon*2, rayon*2);
						gContextBorders.drawRect(i*40, i*40, rayon*2, rayon*2);
						//gContextBorders.drawRect(x, y, width, height);
					}
					else if(forme.getType().equals("OVALE")) {
						gContext.setColor(new Color(0,1,0,0.3f));//GREEN
						
						int rayonH = ((Ovale)forme).getRayonH();
						int rayonV = ((Ovale)forme).getRayonV();
						gContext.fillOval(i*40, i*40, rayonH*2, rayonV*2);
						gContext.setColor(Color.BLACK);
						gContext.drawOval(i*40, i*40, rayonH*2, rayonV*2);
						gContextBorders.drawRect(i*40, i*40, rayonH*2, rayonV*2);
					}
					else { //Si c'est un rectangle ou un carre, la methode pour l'affichage est la meme
						gContext.setColor(new Color(1,0,0,0.5f));//RED
						if(forme.getType().equals("CARRE"))//Change la couleur si le type est carre
							gContext.setColor(new Color(1,1,0,0.3f));//YELLOW
						int longueur = ((Rectangle)forme).getLongueur();
						int hauteur = ((Rectangle)forme).getHauteur();
						gContext.fillRect(i*40, i*40, longueur, hauteur);
						gContextBorders.drawRect(i*40, i*40, longueur, hauteur);
					}
					
				listeFormes.avancerIteration();
				}
			}
		}
	}
	
	/*
	 * Le Layout qui utilise (contient) FenetreFormes doit r�server 
	 * l'espace n�cessaire�� son affichage
	 */
	@Override 
	public Dimension getPreferredSize(){
		return DIMENSION;
	}
}
