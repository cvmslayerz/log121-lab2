package view;
/******************************************************
Cours:  LOG121
Projet: Squelette du laboratoire #1
Nom du fichier: MenuFenetre.java
Date cr��: 2013-05-03
*******************************************************
Historique des modifications
*******************************************************
*@author Patrice Boucher
2013-05-03 Version initiale
2014-09-19 Legere Modification par Etienne Caya
2014-09-02 Modification de l'interface pour correspondre au lab2
*******************************************************/  

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButtonMenuItem;

import model.CommGestion;

/**
 * Cr�e le menu de la fenêtre de l'applicationé
 */
public class MenuFenetre extends JMenuBar{
	
	private static final long serialVersionUID = 1536336192561843187L;
	private static final String
			//Menu Fichier
			MD_TITRE = "app.frame.menus.file.title",
			MD_OBTENIR = "app.frame.menus.file.obtenir",
			//Menu Ordre
			MO_TITRE = 	"app.frame.menus.order.title",
			MO_NSEQ	= 	"app.frame.menus.order.nseq",
			MO_AIRE =	"app.frame.menus.order.aire",
			MO_TYPE	=	"app.frame.menus.order.type",
			MO_DISTANCE	= "app.frame.menus.order.distance",
			//Menu Aide
			MA_TITRE = "app.frame.menus.help.title",
			MA_PROPOS = "app.frame.menus.help.about",
			//Sous menu de Ordre
			SMO_CROISSANT = "app.frame.menus.order.sousMenu.croissant",
			SMO_DECROISSANT = "app.frame.menus.order.sousMenu.decroissant",
			SMO_NORMAL = "app.frame.menus.order.sousMenu.normal",
			SMO_INVERSE = "app.frame.menus.order.sousMenu.inverse";
	private static final String MSG_A_PROPOS = "app.frame.dialog.about";  

	private JMenuItem   demarrerMenuItem, 
						demarrerTriCnseq,
						demarrerTriDnseq,
						demarrerTriCaire,
						demarrerTriDaire,
						demarrerTriN,
						demarrerTriI,
						demarrerTriDist;
 	   
	CommGestion comm; // Pour activer/désactiver la communication avec le serveur
	
	/**
	 * Constructeur
	 */
	public MenuFenetre(CommGestion comm) {
		this.comm = comm;
		addMenuFichier();
		addMenuOrdre();
		addMenuAide();
	}

	/**
	 *  Crée le menu "Draw". 
	 */
	protected void addMenuFichier() {
		JMenu menu = creerMenu(MD_TITRE,new String[] {MD_OBTENIR});

		demarrerMenuItem = menu.getItem(0);
		demarrerMenuItem.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent arg0) {
			//Crée le dialogue demandant les infos nécessaires pour la connexion.
			String input = (String) JOptionPane.showInputDialog(null,"Quel est le nom d'hote et le port du serveur de formes?","Input",JOptionPane.QUESTION_MESSAGE);
			comm.startComm(input);
			rafraichirMenus();
		  }
		});
		
		add(menu);
	}
	
	/**
	 * Crée le menu "Ordre".
	 */
	protected void addMenuOrdre(){
		JMenu menu = creerMenuOrdre(MO_TITRE, new String[] {MO_NSEQ, MO_AIRE, MO_TYPE, MO_DISTANCE});
		
		demarrerTriCnseq = ((JMenu) menu.getMenuComponent(0)).getItem(0);
		demarrerTriCnseq.setSelected(true);
		demarrerTriCnseq.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent event) {
				if(event.getStateChange() == 1) {
					comm.tri("nseqCroissant");
				}
		    }
	    });
		demarrerTriCnseq.setEnabled(false);
		
		demarrerTriDnseq = ((JMenu) menu.getMenuComponent(0)).getItem(1);

		demarrerTriDnseq.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent event) {
				if(event.getStateChange() == 1) {
					comm.tri("nseqDecroissant");
				}
		    }
	    });
		demarrerTriDnseq.setEnabled(false);
		
		/* ************************************** */
		demarrerTriCaire = ((JMenu) menu.getMenuComponent(1)).getItem(0);

		demarrerTriCaire.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent event) {
				if(event.getStateChange() == 1) {
					comm.tri("aireCroissant");
				}
		    }
	    });
		demarrerTriCaire.setEnabled(false);
		
		demarrerTriDaire = ((JMenu) menu.getMenuComponent(1)).getItem(1);

		demarrerTriDaire.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent event) {
				if(event.getStateChange() == 1) {
					comm.tri("aireDecroissant");
				}
			}
	    });
		demarrerTriDaire.setEnabled(false);
		
		/* ************************************** */
		demarrerTriN = ((JMenu) menu.getMenuComponent(2)).getItem(0);
		
		demarrerTriN.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent event) {
				if(event.getStateChange() == 1) {
					comm.tri("formeNormal");
				}
		    }
	    });
		demarrerTriN.setEnabled(false);
		
		demarrerTriI = ((JMenu) menu.getMenuComponent(2)).getItem(1);
		
		demarrerTriI.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent event) {
				if(event.getStateChange() == 1) {
					comm.tri("formeInverse");
				}
		    }
	    });
		demarrerTriI.setEnabled(false);
		
		/* ************************************** */
		demarrerTriDist = menu.getItem(3);
		demarrerTriDist.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent event) {
				if(event.getStateChange() == 1)
					comm.tri("distance");
		    }
	    });
		demarrerTriDist.setEnabled(false);
		
		add(menu);
	}


	/**
	 *  Crée le menu "Help". 
	 */
	private void addMenuAide() {
		JMenu menu = creerMenu(MA_TITRE, new String[] { MA_PROPOS });
		menu.getItem(0).addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null,  LangueConfig.getResource(MSG_A_PROPOS), 
						LangueConfig.getResource(MA_PROPOS), JOptionPane.INFORMATION_MESSAGE);
			}
		});
		add(menu);
	}

	/**
	 *  Activer ou désactiver les items du menu selon la sélection. 
	 */
	private void rafraichirMenus() {
		demarrerTriCnseq.setEnabled(comm.isListePleine());
		demarrerTriDnseq.setEnabled(comm.isListePleine());
		demarrerTriCaire.setEnabled(comm.isListePleine());
		demarrerTriDaire.setEnabled(comm.isListePleine());
		demarrerTriN.setEnabled(comm.isListePleine());
		demarrerTriI.setEnabled(comm.isListePleine());
		demarrerTriDist.setEnabled(comm.isListePleine());
	}
	
	/**
	 * Créer un élément de menu à partir d'un champs principal et ses éléments
	 * @param titleKey champs principal
	 * @param itemKeys éléments
	 * @return le menu
	 */
	private JMenu creerMenu(String titleKey,String[] itemKeys) {
        JMenu menu = new JMenu(LangueConfig.getResource(titleKey));
        for(int i=0; i < itemKeys.length; ++i) {
           menu.add(new JMenuItem(LangueConfig.getResource(itemKeys[i])));
        }
        return menu;
   }
	/**
	 * Crée un élément menu spécialement pour le menu "ordre" qui contient des sous-menus.
	 * @param titleKey champs principal
	 * @param itemKeys éléments
	 * @return le menu
	 */
	private JMenu creerMenuOrdre(String titleKey,String[] itemKeys) {
        JMenu menu = new JMenu(LangueConfig.getResource(titleKey));
        ButtonGroup groupeTri = new ButtonGroup();
        JRadioButtonMenuItem submenuNC = new JRadioButtonMenuItem(LangueConfig.getResource(SMO_CROISSANT));
        JRadioButtonMenuItem submenuND = new JRadioButtonMenuItem(LangueConfig.getResource(SMO_DECROISSANT));
        JRadioButtonMenuItem submenuAC = new JRadioButtonMenuItem(LangueConfig.getResource(SMO_CROISSANT));
        JRadioButtonMenuItem submenuAD = new JRadioButtonMenuItem(LangueConfig.getResource(SMO_DECROISSANT));
        JRadioButtonMenuItem submenuN = new JRadioButtonMenuItem(LangueConfig.getResource(SMO_NORMAL));
        JRadioButtonMenuItem submenuI = new JRadioButtonMenuItem(LangueConfig.getResource(SMO_INVERSE));
        JRadioButtonMenuItem itemDistance = new JRadioButtonMenuItem(LangueConfig.getResource(MO_DISTANCE));
        groupeTri.add(submenuNC);
        groupeTri.add(submenuND);
        groupeTri.add(submenuAC);
        groupeTri.add(submenuAD);
        groupeTri.add(submenuN);
        groupeTri.add(submenuI);
        groupeTri.add(itemDistance);
        
        for(int i=0; i < itemKeys.length; ++i) {
        	if(i!=3)
        		menu.add(new JMenu(LangueConfig.getResource(itemKeys[i])));
        	if(itemKeys[i].equals(MO_NSEQ)){
        		menu.getItem(i).add(submenuNC);
        		menu.getItem(i).add(submenuND);
        	}else if(i==1){
        		menu.getItem(i).add(submenuAC);
        		menu.getItem(i).add(submenuAD);
        	}else if(i==2){
        		menu.getItem(i).add(submenuN);
        		menu.getItem(i).add(submenuI);
        	}
        }
    	menu.add(itemDistance);
    	
        return menu;
   }
}
